<?php
 /**
 *   Copyright (C) 2010 BONNIN Florian (win@warriorhouse.net)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
 
 function pastebin($sCode, $sNick) {
  $aPost = Array(
   'paste_code' => $sCode,
   'paste_name' => '['.$sNick.'] #php-fr @ freenode',
   'paste_private' => 1,
   'paste_expire_date' => '1H',
   'paste_format' => 'php'
  );
  $rContext = stream_context_create(Array(
   'http' => Array(
    'method' => 'POST',
    'header' => 'Content-type: application/x-www-form-urlencoded',
    'content' => http_build_query($aPost),
    'timeout' => 5
   )
  ));
  return file_get_contents('http://pastebin.com/api_public.php', NULL, $rContext);
 }
 $rSocket = fsockopen('irc.freenode.net', 6667);
 if (!$rSocket) die('Impossible de se connecter');
 stream_set_blocking($rSocket, 0);
 fputs($rSocket, "USER phpbot null null :phpbot\n");
 fputs($rSocket, "NICK phpbot\n");
 $sBuffer = $sBufferPhp = $sIrcLine = '';
 $iTimeLaunch = false;
 while (!feof($rSocket)) {
  if ($iTimeLaunch !== false) { // Si php en cours d'execution
   if ((microtime(true)-$iTimeLaunch) > 5) { // Si delais dépassé ou handle php foireux
    proc_terminate($rPhpProc, 9);
    proc_close($rPhpProc);
    fputs($rSocket, $sIrcLine."[Timeout]\n");
    $sIrcLine = $sBufferPhp = '';
    $iTimeLaunch = false;
   }
   else { // Si delais dans les temps
    if (feof($aPipes[1])) { // Si lecture php finie
     $sBufferPhp = str_replace('in Command line code ', '', $sBufferPhp); // On vire le superflu pour la limite de taille IRC
     if ((($iLength = strlen($sBufferPhp)) > 280) && ($iLength < 10240) && preg_match('/^PRIVMSG [^ ]+ :([^>]+)>/i', $sIrcLine, $aRegs)/* Je suis fainéant today :D */) {
      $sBufferPhp = 'Answer too long ('.$iLength.'). Result on '.pastebin($sBufferPhp, $aRegs[1]);
     }
     else {
      $sBufferPhp = preg_replace(Array("/[\r\n]+/", '/\s+/'), Array(' '.chr(2).'--'.chr(2).' ', ' '), trim($sBufferPhp)); // On met en forme pour que ça tienne sur une ligne
     }
     fputs($rSocket, $sIrcLine.substr($sBufferPhp, 0, 280)."\n");
     $sIrcLine = $sBufferPhp = '';
     $iTimeLaunch = false;
     proc_close($rPhpProc);
     sleep(1); // Anti flood
    }
    else if (($sData = fread($aPipes[1], 4096)) !== '') { // Sinon on continue à lire
     $sBufferPhp .= $sData;
    }
   }
  }
  $sData = fgets($rSocket);
  if ($sData !== '') $sBuffer .= $sData;
  if (($iPos = strpos($sBuffer, "\n")) !== false) {
   $sLine = substr($sBuffer, 0, $iPos);
   echo "IRC> {$sLine}\n";
   $sBuffer = substr($sBuffer, $iPos+1);
   if (preg_match('/^PING (.*)$/i', $sLine, $aRegs)) fputs($rSocket, 'PONG '.$aRegs[1]."\n");
   else if (preg_match('/^[^ ]+ 376 /', $sLine, $aRegs)) {
    sleep(1);
    fputs($rSocket, "JOIN #php-fr\n");
   }
   else if (preg_match('/^:(([^!]+)![^ ]+) PRIVMSG (#[^ ]+) :!php (.*)$/', $sLine, $aRegs)) {
    if ($iTimeLaunch === false) {
     $iTimeLaunch = microtime(true);
     $sCode = 'unset($_SERVER,$_ENV,$_COOKIE,$_POST,$_GET,$_FILES,$argc,$argv);'.$aRegs[4].';';
     $rPhpProc = proc_open('exec php -d open_basedir='.getcwd().'/vide/ -c ./php.ini -r '.escapeshellarg($sCode).' 2>&1', array(array('pipe', 'r'), array('pipe', 'w'), array('pipe', 'w')), $aPipes, getcwd(), Array());
     stream_set_blocking($aPipes[1], 0);
     $sIrcLine = 'PRIVMSG '.$aRegs[3].' :'.$aRegs[2].'> ';
    }
   }
  }
  usleep(10000);
 }
?>